# EEE3088F Solar Panel Project
# This repo includes

This repo includes the entire history of the project development from beginning to end,containing all files for production and to allow for edits to the design. 

The function of this board is to determine which direction the most sunlight will be coming in from and then indicated through the use of 4 LEDS in what direction the solar panels needs to be moved to for maximum sunlight. Temperature data is also recorded and stored on an onboard memory device.

# Hardware Required
This project is designed off the STM32 Discovery Board, this is the brain of the project,
Jumper cables,
A single cell 1860 battery holder,
Lithium battery,
16 pin connectors *2,

# Software/tools
To make edits to the existing software code presented in this respiratory, users will need to download STM32CubeIDE 1.9.0. 

# Connecting the hardware
Due to a miscalculation in distance of the pins on the discovery board users would have to connect the discovery board to the hat via external jumper wires. This can be avoided if on the template of the PCB layout the pin connectors footprints are adjusted to the right distance apart. The lithium battery holder will need to be hand soldered onto its respective footprint. If required the pin connectors can also be soldered on.

# How to run software
Software is all pre-set and no user intervention is required, simply download the template code in the code filer under main branch and run to board. 

# License
GNU General Public License v2.0	gpl-2.0
