This project is accepting for contributions of improvements, updates and fixes on bugs and errors. These contributions are welcome via Git or email on solarpanelhat@gmail.com. 
Reporting Bugs
A Bug Report:
•	Check the faq and the discussions for a list of common questions and problems.
•	Determine which repository the problem should be reported in.
•	Perform a cursory search to see if the problem has already been reported. If it has and the issue is still open, add a comment to the existing issue instead of opening a new one.
Suggesting Enhancements/Updates
Before Submitting An Enhancement Suggestion
•	Check if there's already a package which provides that enhancement.
•	Determine which repository the enhancement should be suggested in.
•	Perform a cursory search to see if the enhancement has already been suggested. If it has, add a comment to the existing issue instead of opening a new one.
How Do I Submit A (Good) Enhancement Suggestion?
Enhancement suggestions are tracked as GitHub issues. After you've determined which repository your enhancement suggestion is related to, create an issue on that repository and provide the following information:
•	Use a clear and descriptive title for the issue to identify the suggestion.
•	Provide a step-by-step description of the suggested enhancement in as many details as possible.
•	Provide specific examples to demonstrate the steps. Include copy/pasteable snippets which you use in those examples.
•	Describe the current behaviour and explain which behaviour you expected to see instead and why.
